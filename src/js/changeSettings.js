
module.exports = class changeSettings {

  constructor(options){
    let defaults = {
      changeBtn: '.change',
    };

    let populated = Object.assign(defaults, options);
    for(let key in populated) {
      if(populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }

    this.start(this);
  }

  start(that){

    let changeBtn = document.querySelector(that.changeBtn);
    // console.log(buttons);
    if(changeBtn){
      changeBtn.addEventListener("click", function (event) {
        alert("Your changes has successfuly saved.");
        event.preventDefault();
      });
    }
  }

};
