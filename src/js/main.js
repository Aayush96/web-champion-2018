require('../../src/scss/_main.scss');

import objectFitImages from 'object-fit-images';

objectFitImages();

 require('slick-carousel');

import SlickSlider from './SlickSlider.js';

new SlickSlider();
new SlickSlider({sliderClass: '.slideshow-container', sm: 1, md: 1, lg: 1, xl: 1});

import chooseHero from './chooseHero.js';
new chooseHero();

import level1 from './level1.js';
new level1();

import changeSettings from './changeSettings.js';
new changeSettings();
