module.exports = class level1 {

  constructor(options) {
    let defaults = {
      nextBtn: '.level1__next',
      levelSectionClass: '.level__section',
      checkBtn: '.gameplay__check'
    };

    let populated = Object.assign(defaults, options);
    for (let key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }

    this.next(this);
    this.checkAnswer(this);
  }

  next(that) {
    let nextBtns = document.querySelectorAll(that.nextBtn);
    let levelSections = document.querySelectorAll(that.levelSectionClass);
    if (nextBtns) {
      let levelSectionsSize = levelSections.length;
      for (let i = 0; i < nextBtns.length; i++) {

        nextBtns[i].addEventListener("click", function(event) {
          let currentlevelSection = event.target.parentNode.parentNode;

          //hiding current section
          currentlevelSection.classList.add(`level1__hidden`);

          var nextIndex = i + 1;
          var nextLevelSection = null;
          if (nextIndex < levelSectionsSize) {
            nextLevelSection = levelSections[i + 1];
          } else {
            nextLevelSection = levelSections[i];
          }

          //showing intended section
          nextLevelSection.classList.remove(`level1__hidden`);
        });

      }
    }
  }

  checkAnswer(that){
    let checkBtn = document.querySelector(that.checkBtn);
    var dialogueBox = document.querySelector('.gameplay__dialogue');
    // console.log(dialogueBox);
    var output = document.querySelector('.gameplay__output');
    var error = document.createElement("p");
    error.classList.add('form__error');

    if(checkBtn && output){
      checkBtn.addEventListener("click", function(event) {
        var gameIframe = output.contentWindow.document;
        console.log("event");


        var head = gameIframe.querySelector('head');
          // console.log(head);
        var body = gameIframe.querySelector('body');
          // console.log(body);
        var h1 = gameIframe.querySelector('h1');
          // console.log(h1);
        var p = gameIframe.querySelector('p');
          // console.log(p);

        if(head === null){
          error.textContent = "You havent added head tag";
          dialogueBox.appendChild(error);
          // console.log("event head");

        }
        else if(body === null){
          error.textContent = "You havent added body tag";
          dialogueBox.appendChild(error);
          // console.log("event body");

        }
        else if(h1 === null){
          error.textContent = "You havent added h1 tag";
          dialogueBox.appendChild(error);
          // console.log("event h1");

        }
        else if(p === null){
          error.textContent = "You havent added p tag";
          dialogueBox.appendChild(error);
          // console.log("event p");
        } else {
          let levelSections = document.querySelectorAll(that.levelSectionClass);

          let winSectionIndex = levelSections.length - 1;

          let currentlevelSection = dialogueBox.parentNode;

          let nextLevelSection = levelSections[winSectionIndex];

          //hiding current section
          currentlevelSection.classList.add(`level1__hidden`);
          nextLevelSection.classList.remove(`level1__hidden`);

        }

      });

    }
  }
};
