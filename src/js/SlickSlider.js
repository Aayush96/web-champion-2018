
module.exports = class SlickSlider {

  constructor(options){
    let defaults = {
      sliderClass: '.characters__grid',
      sm: 1,
      md: 1.5,
      lg: 2.1,
      xl: 3,
      showDots: true
    };

    let populated = Object.assign(defaults, options);
    for(let key in populated) {
      if(populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }

    this.start(this);
  }

  start(that){
    var sliderItem = $(that.sliderClass);
    if(sliderItem){
      sliderItem.slick({
        infinite: true,
        centerMode: true,
        focusOnSelect: true,
        dots: that.showDots,
        slidesToShow: that.xl,
        slidesToScroll: 1,
        centerPadding: '40px',
        nextArrow: "<button class='slick-controls slick-arrow slick-controls--right '>></button>",
        prevArrow: "<button class='slick-controls slick-arrow slick-controls--left '><</button>",
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: that.lg
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: that.md
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: that.sm
            }
          }
        ]
      });
    }
  }

};
