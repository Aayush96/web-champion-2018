
module.exports = class chooseHero {

  constructor(options){
    let defaults = {
      chooseBtn: '.choose',
    };

    let populated = Object.assign(defaults, options);
    for(let key in populated) {
      if(populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }

    this.start(this);
  }

  start(that){

    let buttons = document.querySelectorAll(that.chooseBtn);
    // console.log(buttons);
    if(buttons){
      Array.prototype.forEach.call(buttons, function(button){
        button.addEventListener("click", function (event) {
          alert("Your hero {heroName} has been choosen.");
          event.preventDefault();
        });
      });
    }
  }

};
