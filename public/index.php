<?php
//display errors control
// incase it is off for dev and on for production
ini_set('display_errors','off');
error_reporting(E_ALL);
//path to add before every url in remote
define("RPATH", "/webchampionv4");

define("ROOT", $_SERVER['DOCUMENT_ROOT']);
define("LIB",ROOT.RPATH."/lib");
define("VIEWS", LIB."/views");

//path to static files
define("APP", LIB."/application.php");
define("CSSPATH", RPATH."/build/main.bundle.css");
define("JSPATH", RPATH."/build/main.bundle.js");

//path to model file
define("MODEL", LIB."/model.php");

require APP;
require MODEL;

$messages = array();



get(RPATH."/", function(){
  urlBaseHttp("/");
  $messages["title"] = "Home";
  render("master", "home", $messages);
});

get(RPATH."/home", function(){
  urlBaseHttp("/");
  $messages["title"] = "Home";
  render("master", "home", $messages);
});

get(RPATH."/game", function(){
  if(is_authenticated()){
    urlBaseHttp("/game");
    $messages["title"] = "Game";
    render("master", "game", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/levels", function(){
  if(is_authenticated()){
    urlBaseHttp("/levels");
    $messages["title"] = "Levels";
    render("master", "levels",  $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/game/achievements", function(){
  if(is_authenticated()){
    urlBaseHttp("/game/achievements");
    $id = $_SESSION["id"];
    $messages["title"] = "Achievements";
    $messages["user_name"] = get_user_name($id);
    render("master", "achievements", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/help", function(){
  urlBaseHttp("/help");
  $messages["title"] = "Help";
  render("master", "help", $messages);
});

get(RPATH."/game/settings", function(){
  if(is_authenticated()){
    urlBaseHttp("/settings");
    $messages["title"] = "Settings";
    $id = $_SESSION["id"];
    $messages["user_name"] = get_user_name($id);
    render("master", "settings", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/character", function(){
  if(is_authenticated()){
    urlBaseHttp("/character");
    $messages["title"] = "Character";
    render("master", "character", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/level/1", function(){
  if(is_authenticated()){
    urlBaseHttp("/level/1");
    $messages["title"] = "Level 1";
    $id = $_SESSION["id"];
    $messages["user_name"] = get_user_name($id);
    render("master", "level1", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/user/setting", function(){
  if(is_authenticated()){
    urlBaseHttp("/accountSettings");
    $id = $_SESSION["id"];
    $messages["title"] = "Settings";
    $messages["user_name"] = get_user_name($id);
    render("master", "accountSetting", $messages);
  } else {
    set_flash("Insufficient access. Please sign in!");
    redirect_to(RPATH."/");
  }
});

get(RPATH."/signup", function(){
  urlBaseHttp("/signup");
  $messages["title"] = "Sign up";
  render("master", "signup", $messages);
});

post(RPATH."/",function(){
  $name = form('user_name');
  $password = form('password');
  if($name && $password){
    try {
      sign_in($name,$password);
    }
    catch(Exception $e){
      set_flash($e->getMessage());
      redirect_to(RPATH."/");
    }
  }
  else{
    set_flash("Invalid user");
    redirect_to(RPATH."/");
  }
  redirect_to(RPATH."/game");
});


post(RPATH."/signup",function(){

  try{
    $name = form('username');
    $pw = form('password');
    $confirm = form('password-confirm');

    if($name && $pw && $confirm){
      try {
        sign_up($name, $pw, $confirm);

      }
      catch(Exception $e){
        set_flash($e->getMessage());
        redirect_to(RPATH."/signup");
      }
      try {
        sign_in($name, $pw);

      } catch(Exception $e){
        set_flash("Sorry, there was a problem with login");
        redirect_to(RPATH."/signup");
      }
    }
    else {
      set_flash("Please, fill the fields with appropriate information");
      redirect_to(RPATH."/signup");
    }
  }
  catch(Exception $e){
    set_flash($e->getMessage());
    redirect_to(RPATH."/signup");
  }
  redirect_to(RPATH."/game");

});

post(RPATH."/signout",function(){
  sign_out();
  redirect_to(RPATH."/");
});


?>
