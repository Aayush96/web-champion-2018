<?php

//handle all types of request
function get($route, $callback){
  if($route === get_request() && get_method() === "GET"){
    echo $callback($route);
  } else {
    return false;
  }
}

function post($route, $callback){
  if($route === get_request() && get_method() === "POST"){
    echo $callback($route);
  } else {
    return false;
  }
}

function put($route, $callback){
  if($route === get_request() && get_method() === "PUT"){
    echo $callback($route);
  } else {
    return false;
  }
}

function delete($route, $callback){
  if($route === get_request() && get_method() === "DELETE"){
    echo $callback($route);
  } else {
    return false;
  }
}

//renderers
function error_404($callback){
  echo $callback();
}

//render page

function render($layout, $page, $messages){

  foreach((array) $messages As $key => $val){
      $$key = $val;
  }

  if(!empty($layout)){
    require(VIEWS."/{$layout}.layout.php");
  } else {
    echo "We have a fatal error";
    exit();
  }
}

function urlBaseHttps($path="/"){
  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
    $host = $_SERVER['HTTP_HOST'];
    $redirect_to_path = "https://".$host.$path;
    redirect_to($redirect_to_path);
    exit();
  }
}

function urlBaseHttp($path="/"){
  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=== 'on'){
    $host = $_SERVER['HTTP_HOST'];
    $redirect_to_path = "http://".$host.$path;
    redirect_to($redirect_to_path);
    exit();
  }
}


//helpers
function get_request() {
  return $_SERVER['REQUEST_URI'];
}

function get_method(){
  $request_method = "GET";

  if(!empty($_SERVER['REQUEST_METHOD'])){
    $request_method = strtoupper($_SERVER['REQUEST_METHOD']);
  }

  if($request_method === "POST"){
    if(strtoupper(form("_method")) === "POST"){
      return "POST";
    }
    if(strtoupper(form("_method")) === "PUT"){
      return "PUT";
    }
    if(strtoupper(form("_method")) === "DELETE"){
      return "DELETE";
    }
    return "POST";
  }
  if($request_method === "PUT"){
    return "PUT";
  }

  if($request_method === "DELETE"){
    return "DELETE";
  }

  return "GET";
}

function form($key){
  if(!empty($_POST[$key])){
    return $_POST[$key];
  }
  return true;
}

function redirect_to($path = "/"){
  header("Location: {$path}");
  exit();
}

function set_session_message($key,$message){
   if(!empty($key) && !empty($message)){
      session_start();
      $_SESSION[$key] = $message;
      session_write_close();
   }
}

function get_session_message($key){
   $msg = "";
   if(!empty($key) && is_string($key)){
      session_start();
      if(!empty($_SESSION[$key])){
         $msg = $_SESSION[$key];
         unset($_SESSION[$key]);
      }
      session_write_close();
   }
   return $msg;
}

function set_flash($msg){
      set_session_message("flash",$msg);
}

function get_flash(){
      return get_session_message("flash");
}



?>
