USE webchampion_db;

CREATE TABLE `game` (
  gameid int(100) NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  level int(3) NOT NULL,
  points int(3) NOT NULL,
  
  PRIMARY KEY (gameid)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

