

<section class="menu-page wrap">
  <div class="container">
    <div class="user_setting">
      <h2 class="menu-page__title">User Setting</h2>
      <div class="user-details">

        <img class="user-details__img" src="http://placehold.it/200x180" alt="#">
        <div class="user-details__text rte">
          <div class="user-details__text-wrap">
            <h3 class="user-details__title">Name</h3>
            <p><?php echo $user_name;?></p>
          </div>
          <div class="user-details__text-wrap">
            <h3 class="user-details__title">Email address</h3>
            <p>96.estd@gmail.com</p>
          </div>
          <div class="user-details__text-wrap">
            <h3 class="user-details__title">Reminder time to code</h3><p>5pm</p>
          </div>
          <p>brief detail about user level and character level he has passed so far and other insights</p>
          <a href="/user/setting" class="btn">Change</a>
        </div>
      </div>
      <h2 class="menu-page__title">Game Settings</h2>
      <div class="menu-page__header">
        <form class="form__controls form" action="" method="post">
          <div class="form__control">
            <label class="form__label" for="Sound">Sound</label>
            <input class="form__input form__input--range" type="range" min="0" max="100">
          </div>
          <p class="form__label">Graphics</p>
          <div class="form__control">

            <input class="form__radio-input" type="radio" name="radio" value="this">
            <label class="form__radio-label" for="radio">Very Good (uses more memory)</label>

            <input class="form__radio-input" type="radio" name="radio" value="this">
            <label class="form__radio-label" for="radio">Good</label>
          </div>

        </form>
      </div>
    </div>
  </section>
  <section class="characters">
    <h2 class="characters__title">Characters</h2>
    <p class="characters__instruction">click on cards to go to character settings.<strong> This is also meant to be slider</strong></p>
    <div class="characters__grid">
      <div class="character-details characters__item">
        <img class="character-details__img" src="./img/Ashe.png" alt="">

        <div class="character-details__text">
          <h2 class="character-details__title">Ashe</h2>
          <p class=" character-details__rating starability-result" data-rating="3">
            Rated: 3 stars
          </p>
          <p>brief detail about character level and character level he has passed so far and other insights</p>
          <div class="character-details__cta">
            <button type="submit" name="choose" class="btn choose">Choose</button>
          </div>
        </div>
      </div>
      <div class="character-details characters__item">
        <img class="character-details__img" src="./img/Elise.png" alt="">
        <div class="character-details__text">
          <h2 class="character-details__title">Elise</h2>
          <p class=" character-details__rating starability-result" data-rating="3">
            Rated: 3 stars
          </p>
          <p>brief detail about character level and character level he has passed so far and other insights</p>
          <div class="character-details__cta">
            <button type="submit" name="choose" class="btn choose">Choose</button>
          </div>
        </div>
      </div>
      <div class="character-details characters__item">

        <img class="character-details__img" src="./img/Darius.png" alt="">
        <div class="character-details__text">
          <h2 class="character-details__title">Darius</h2>
          <p class=" character-details__rating starability-result" data-rating="4">
            Rated: 4 stars
          </p>
          <p>brief detail about character level and character level he has passed so far and other insights</p>
          <div class="character-details__cta">
            <button type="submit" name="choose" class="btn choose">Choose</button>
          </div>
        </div>
      </div>
      <div class="character-details characters__item">
        <img class="character-details__img" src="./img/Fiora.png" alt="">
        <div class="character-details__text">
          <h2 class="character-details__title">Fiora</h2>
          <p class=" character-details__rating starability-result" data-rating="5">
            Rated: 5 stars
          </p>
          <p>brief detail about character level and character level he has passed so far and other insights</p>
          <div class="character-details__cta">
            <button type="submit" name="choose" class="btn choose">Choose</button>
          </div>
        </div>
      </div>
      <div class="character-details characters__item">
        <img class="character-details__img" src="./img/Garen.png" alt="">
        <div class="character-details__text">
          <h2 class="character-details__title">Garen</h2>
          <p class=" character-details__rating starability-result" data-rating="4">
            Rated: 4 stars
          </p>
          <p>brief detail about character level and character level he has passed so far and other insights</p>
          <div class="character-details__cta">
            <button type="submit" name="choose" class="btn choose">Choose</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="menu-page__btn-wrap btn-wrap">
    <a href="<?php echo RPATH;?>/game" class="btn">Go Back</a>
  </div>
