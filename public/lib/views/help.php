
<main class="wrap">
	<section class="container rte">

		<h1>Need help?</h1>
			<h3>Introduce the game and its features.</h3>
			<p>
				Web Champion is aim to help you become a web developer and learn the skill from scratch.
				The game includes comprehensive knowledge on HTML, CSS and Javascript.It will help you understand
			  these knowledge in depth and master them through playing our game.
			</p>
			<p>
				The main features of the game are role play and level-up. We provide amazing hero selections for you,
				You can choose one of them and start pretending you are who you chosen in the game. The only way to
				get into next level is by completing the current level successfuly.
			</p>


			<h3>How to play the game.</h3>
			<p>
				Once you create an account, you can start building your web development skill by exploring our
				level-up game. You can choose the character you like and the character will represent you in the
				game. After completing each level, there will be coins going into your account. Don't waste them,
				use the coins to buy outfits for your character.
			</p>

			<h3>How you communicate with others through the game</h3>
			<p>
				We are currently planning to build a multiplayer mode in the next version. And you can share your
				achievement with others.
			</p>
	</section>
</main>
