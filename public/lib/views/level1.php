
<section class="gameplay wrap" >
  <div class="container">
    <div class="gameplay__wrapper">
      <article class="level__section">
        <img src="<?php echo RPATH;?>/img/storyteller.png" alt="" class="storyteller">
        <div class="storyteller__dialouge">
          <h1 class="storyteller__dialouge-title">Welcome to Web Champion, <?php echo $user_name;?>!</h1>
          <p>You are now on the path to becoming the best web developer in the world!
            This is the very first level where you will learn the basic structure of HTML!
          </p>
          <button class="level1__next btn btn--small">Continue</button>
        </div>
        <div class="btn-wrap">
          <a href="<?php echo RPATH;?>/levels" class="btn">Back to levels</a>
        </div>
      </article>

      <article class="level1__hidden level__section">
        <div class="storyteller__img-wrap">
          <img src="<?php echo RPATH;?>/img/storytellerRight.png" alt="" class="storyteller storyteller--left">
        </div>

        <div class="storyteller__dialouge storyteller__dialouge--left">
          <p>Are you ready for the first challenge, <?php echo $user_name;?>? If so, press continue! </p>
          <button class="level1__next btn btn--small">Continue</button>
        </div>
        <div class="btn-wrap">
          <a href="<?php echo RPATH;?>/levels" class="btn">Back to levels</a>
        </div>
      </article>

      <article class="level1__hidden level__section">
        <div class="storyteller__dialouge gameplay__dialogue">
          <p>Write-up a HTML page with the tags &lt;html&gt;, &lt;head&gt;,
            &lt;body&gt;, &lt;h1&gt; and &lt;p&gt;, and see what it outputs!</p>
          </div>
          <br>
          <div class="htmloutput">
            <div class="gameplay__content">
              <div class="tag">Write HTML here:</div>
              <div id="html" class="content" contenteditable></div>
            </div>
            <div class="gameplay__content">
              <div class="tag">Output:</div>
              <iframe id="output" class="gameplay__output"></iframe>
              <button class="gameplay__check btn btn--small">Check answer</button>
            </div >
          </div>

          <div class="btn-wrap">
            <a href="<?php echo RPATH;?>/levels" class="btn">Back to levels</a>
          </div>
          <script>
          window.onload = function() {
            var html = document.getElementById("html"),
            output = document.getElementById("output"),
            working = false,
            fill = function() {
              if (working) {
                return false;
              }
              working = true;
              var document = output.contentDocument;
              document.body.innerHTML = html.textContent;
              // style.innerHTML = css.textContent.replace(/\s/g, "");
              // script.innerHTML = js.textContent;
              // document.body.appendChild(style);
              // document.body.appendChild(script);
              working = false;
            };
            html.onkeyup = fill;
            // css.onkeyup = fill;
            // js.onkeyup = fill;
          }
          </script>
        </article>

        <article class="level1__hidden level__section">
          <img src="/img/storyteller.png" alt="" class="storyteller">

          <div class="storyteller__dialouge storyteller__dialouge">
            <p><strong>YAY!!! Congratulations, <?php echo $user_name;?></strong>, now you know how to make a simple html page and you have basic knowledge of how tags work.</p>
            <a href="/game/levels" class="btn">Back to levels</a>
        </article>


      </div>
    </div>
  </section>
