<section class="menu-page wrap">
  <div class="container">
    <div class="menu-page__header">
      <h1 class="menu-page__title">Achievements</h1>
      <div class="user-details">
        <img class="user-details__img" src="http://placehold.it/200x180" alt="#">
        <div class="user-details__text">
          <h2 class="user-details__title"><?php echo $user_name;?></h2>
          <p>brief detail about user level and character level he has passed so far and other insights</p>
        </div>
      </div>
      <div class="achievements__wrap">
        <h2 class="achievements__title">Achievements unlocked so far</h2>
        <ul class="achievements">
          <li class="achievements__item">Completed level 1</li>
          <li class="achievements__item">Completed level 2</li>
          <li class="achievements__item">Web Beginner</li>
          <li class="achievements__item">Not unlocked yet!</li>
          <li class="achievements__item">Not unlocked yet!</li>
        </ul>
      </div>

      <div class="menu-page__btn-wrap btn-wrap">
        <a href="<?php echo RPATH;?>/game" class="btn">Go Back</a>
      </div>
    </div>
  </div>
</section>
