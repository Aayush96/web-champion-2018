<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?> | Web Champion</title>
  <link rel="stylesheet" href="<?php echo CSSPATH;?>" />
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
</head>
<body>
  <header>
    <div id="mobile-bar"></div>
    <input type="checkbox" class="check" id="checked">
    <label class="menu-btn" for="checked">
      <span class="bar top"></span>
      <span class="bar middle"></span>
      <span class="bar bottom"></span>
      <span class="menu-btn__text"></span>
    </label>
    <label class="close-menu" for="checked"></label>
    <nav class="drawer-menu">
      <ul>

        <li><a href="<?php echo RPATH;?>/home">Home</a></li>
        <?php if(is_authenticated()){ ?>
          <li><a href="<?php echo RPATH;?>/game">Play</a></li>
          <li><a href="<?php echo RPATH;?>/game/settings">Settings</a></li>
          <li><a href="<?php echo RPATH;?>/game/achievements">Achievements</a></li>
        <?php } ?>
        <li><a href="<?php echo RPATH;?>/help">Help</a></li>
        <?php if(is_authenticated()){ ?>
          <li><form action="<?php echo RPATH;?>/signout" method="post"><input type="hidden" method="POST" name="_method"><button class="btn"></form>Log out</button></li><?php
        } else {?>
          <li> <a href="<?php echo RPATH;?>/signup">Sign up</a> </li>
        <?php }?>
      </ul>
    </nav>

    <div id="desk-nav">
      <div class="container">

      <a href="<?php echo RPATH;?>/">
        <img class="desk-nav__logo" src="http://placehold.it/100x80" alt="Logo">
      </a>
      <nav class="desk-nav">
        <ul>
          <li><a href="<?php echo RPATH;?>/home">Home</a></li>
          <?php if(is_authenticated()){ ?>
            <li><a href="<?php echo RPATH;?>/game">Play</a></li>
            <li><a href="<?php echo RPATH;?>/game/settings">Settings</a></li>
            <li><a href="<?php echo RPATH;?>/game/achievements">Achievements</a></li><?php
          }?>
          <li><a href="<?php echo RPATH;?>/help">Help</a></li>
          <?php if(is_authenticated()){ ?>
            <li><form action="<?php echo RPATH;?>/signout" method="post"><input type="hidden" value="POST" name="_method"><button class="btn"></form>Log out</button></li>
          <?php } else {?>
            <li> <a href="<?php echo RPATH;?>/signup">Sign up</a> </li>
          <?php }?>
        </ul>
      </nav>
    </div>
    </div>
  </header>
