<section class="level wrap">
  <div class="container">
    <h2 class="level__title">Choose a level</h2>
    <div class="level__grid">

      <a href="<?php echo RPATH;?>/level/1" class="level__grid-item">
        <img class="level__item-img" src="http://placehold.it/400x400" alt="#">
        <div class="level__item-text">
          <p>level 1</p>
        </div>
      </a>
      <a href="<?php echo RPATH;?>/level/1" class="level__grid-item">
        <img class="level__item-img" src="http://placehold.it/400x400" alt="#">
        <div class="level__item-text">
          <p>level 2</p>
        </div>
      </a>
      <a href="<?php echo RPATH;?>/level/1" class="level__grid-item">
        <img class="level__item-img" src="http://placehold.it/400x400" alt="#">
        <div class="level__item-text">
          <p>level 3</p>
        </div>
      </a>
      <a href="<?php echo RPATH;?>/level/1" class="level__grid-item">
        <img class="level__item-img" src="http://placehold.it/400x400" alt="#">
        <div class="level__item-text">
          <p>level 4</p>
        </div>
      </a>
      <a href="<?php echo RPATH;?>/level/1" class="level__grid-item">
        <img class="level__item-img" src="http://placehold.it/400x400" alt="#">
        <div class="level__item-text">
          <p>level 1</p>
        </div>
      </a>

    </div>
    <div class="menu-page__btn-wrap btn-wrap">
      <a href="<?php echo RPATH;?>/game" class="btn">Go Back</a>
    </div>
  </div>
</section>
