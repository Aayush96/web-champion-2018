<main >
	<div class="main_body">
		<div class="container">
			<div class="website_motto">
				<h1>Web Champion</h1>
				<p>Play, Win and Learn</p>
				<p>Web Champion is a role palying game and it will blow your head off. This is absolute and also a test of text that is going to be here.</p>
				<div class="btn__wrap--center">
					<a class="btn" href="#onboarding">View Guide</a>
				</div>
			</div>
			<div class="signin">
				<?php if(is_authenticated())
						{
					?>
					<div class="sign_form">
					<h3>Already signed in</h3>
					<br>
					<a href="<?php echo RPATH;?>/game" class="btn">Play Now</a>
					</div>
					<?php
					}
				else
				 { ?>
				<form class="sign_form" action="<?php echo RPATH;?>/" method="POST">
					<fieldset>
						<legend>Play now!</legend>

						<?php
						$error = get_flash();

						if($error != "") {
							echo "<p class='form__error'>";
							echo $error;
							echo "</p>";
						}
						?>

						<input type="hidden" name="_method" value="POST">
						<div class="user_input">
							<label class="form__label" for="username">User Name</label>
							<input class="form__input" type="text" id="user_name" name="user_name" required>
							<p class="form__input-error">Please, provide a valid username </p>
						</div>

						<div class="user_input">
							<label class="form__label" for="password">Password</label>
							<input class="form__input" type="password" id="password" name="password" required>
							<p class="form__input-error">Please, provide a valid password </p>
						</div>

						<button class="btn" type="submit">Sign In</button>
						<br>
						<div class="rte">
							<a href="<?php echo RPATH;?>/signup">Dont have a account yet?</a>

						</div>

					</fieldset>
				</form>
			<?php } ?>
			</div>
		</div>
	</div>

	<div class="slideshow-container" id='onboarding'>

		<!-- Full-width images with number and caption text -->
		<div class="mySlides fade">
			<h3>Introduce the game and its features.</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		</div>

		<div class="mySlides fade">
			<h3>How to play the game.</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>

		<div class="mySlides fade">
			<h3>How you communicate with others through the game</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
