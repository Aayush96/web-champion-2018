
<main class="usersetting_main wrap">
	<div class="user_setting">
		<h2>User Setting</h2>
		<form class="" action="<?php echo RPATH;?>/user/setting" method="post">
			<div class="user_input">
				<label class="form__label" for="username">User Name:</label>
				<input class="form__input" type="text" id="user_name" name="user_name" autofocus required value="<?php echo $user_name;?>">
			</div>

			<div class="user_input">
				<label class="form__label" for="email">Email:</label>
				<input class="form__input" type="email" id="email" name="email" required placeholder="batman@goodman.com">
			</div>

			<div class="user_input">
				<label class="form__label" for="reminder">Time to remind for coding everyday:</label>
				<input class="form__input" type="text" id="reminder" name="reminder" required placeholder="5pm">
			</div>

			<button class="btn change" type="submit">Change</button>
		</form>
	</div>
</main>
