
<main class="wrap">
	<div class="container">
		<form class="sign_form" method="POST" action="<?php echo RPATH;?>/signup">
			<h1>Sign Up!</h1>

			<?php
			$error = get_flash();

			if($error != "") {
				echo "<p class='form__error'>";
				echo $error;
				echo "</p>";
			}
			?>

			<div class="user_input">
				<label class="form__label" for="username">User Name</label>
				<input class="form__input" type="text" id="username" name="username" required autofocus>
				<p class="form__input-error">Please, provide a valid username</p>
			</div>


			<div class="user_input">
				<label class="form__label" for="password">Password</label>
				<input class="form__input" type="password" id="password" name="password" required>
				<p class="form__input-error">Please, provide a valid password</p>
			</div>

			<div class="user_input">
				<label class="form__label" for="password-confirm">Confirm Password</label>
				<input class="form__input" type="password" id="password-confirm" name="password-confirm" required>
				<p class="form__input-error">Please match the password</p>
			</div>

			<div class="user_input sign_button">
				<button type="submit" class="btn">Create Account</button>
			</div>

		</form>
	</div>
</main>
