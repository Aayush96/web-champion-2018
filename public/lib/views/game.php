<section class="menu-page">
  <div class="container wrap">
    <div class="menu-page__header">
      <h1 class="menu-page__title">Web Champion</h1>
      <img class="menu-page__img" src="http://placehold.it/700x400" alt="#">
    </div>
    <div class="menu-page__btn-wrap btn-wrap">
      <a href="<?php echo RPATH;?>/levels" class="btn">Start</a>
      <a href="<?php echo RPATH;?>/game/achievements" class="btn">Achievements</a>
      <a href="<?php echo RPATH;?>/game/settings" class="btn">Settings</a>
      <a href="<?php echo RPATH;?>/help" class="btn">Help</a>
    </div>
  </div>
</section>
